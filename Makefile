.PHONY: help

help: ## Displays the available commands (this help)
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

run: ## Runs application
	@./gradlew run

test: ## Runs all the tests in the project
	@./gradlew test --rerun-tasks

check: ## Runs all verification tasks
	@./gradlew check --rerun-tasks

docker-build: ## Builds Docker image
	@./gradlew dockerBuildImage
