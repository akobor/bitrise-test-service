/*
 * This file is generated by jOOQ.
 */
package io.bitrise.test.sql.tables;


import io.bitrise.test.sql.DefaultSchema;
import io.bitrise.test.sql.Indexes;
import io.bitrise.test.sql.Keys;
import io.bitrise.test.sql.tables.records.AppRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class App extends TableImpl<AppRecord> {

    private static final long serialVersionUID = 1180699583;

    /**
     * The reference instance of <code>APP</code>
     */
    public static final App APP = new App();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<AppRecord> getRecordType() {
        return AppRecord.class;
    }

    /**
     * The column <code>APP.ID</code>.
     */
    public final TableField<AppRecord, Integer> ID = createField(DSL.name("ID"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>APP.USER_ID</code>.
     */
    public final TableField<AppRecord, Integer> USER_ID = createField(DSL.name("USER_ID"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>APP.PUBLIC</code>.
     */
    public final TableField<AppRecord, Boolean> PUBLIC = createField(DSL.name("PUBLIC"), org.jooq.impl.SQLDataType.BOOLEAN.nullable(false).defaultValue(org.jooq.impl.DSL.field("FALSE", org.jooq.impl.SQLDataType.BOOLEAN)), this, "");

    /**
     * The column <code>APP.USE_PLAN_LIMITS</code>.
     */
    public final TableField<AppRecord, Boolean> USE_PLAN_LIMITS = createField(DSL.name("USE_PLAN_LIMITS"), org.jooq.impl.SQLDataType.BOOLEAN.nullable(false).defaultValue(org.jooq.impl.DSL.field("TRUE", org.jooq.impl.SQLDataType.BOOLEAN)), this, "");

    /**
     * The column <code>APP.CONCURRENT_BUILD_LIMIT</code>.
     */
    public final TableField<AppRecord, Short> CONCURRENT_BUILD_LIMIT = createField(DSL.name("CONCURRENT_BUILD_LIMIT"), org.jooq.impl.SQLDataType.SMALLINT, this, "");

    /**
     * The column <code>APP.BUILD_TIME_MINUTES_LIMIT</code>.
     */
    public final TableField<AppRecord, Short> BUILD_TIME_MINUTES_LIMIT = createField(DSL.name("BUILD_TIME_MINUTES_LIMIT"), org.jooq.impl.SQLDataType.SMALLINT, this, "");

    /**
     * The column <code>APP.MONTHLY_BUILD_LIMIT</code>.
     */
    public final TableField<AppRecord, Short> MONTHLY_BUILD_LIMIT = createField(DSL.name("MONTHLY_BUILD_LIMIT"), org.jooq.impl.SQLDataType.SMALLINT, this, "");

    /**
     * The column <code>APP.TEAM_MEMBER_LIMIT</code>.
     */
    public final TableField<AppRecord, Short> TEAM_MEMBER_LIMIT = createField(DSL.name("TEAM_MEMBER_LIMIT"), org.jooq.impl.SQLDataType.SMALLINT, this, "");

    /**
     * Create a <code>APP</code> table reference
     */
    public App() {
        this(DSL.name("APP"), null);
    }

    /**
     * Create an aliased <code>APP</code> table reference
     */
    public App(String alias) {
        this(DSL.name(alias), APP);
    }

    /**
     * Create an aliased <code>APP</code> table reference
     */
    public App(Name alias) {
        this(alias, APP);
    }

    private App(Name alias, Table<AppRecord> aliased) {
        this(alias, aliased, null);
    }

    private App(Name alias, Table<AppRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> App(Table<O> child, ForeignKey<O, AppRecord> key) {
        super(child, key, APP);
    }

    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.APP_USER_ID, Indexes.CONSTRAINT_INDEX_F, Indexes.PRIMARY_KEY_F);
    }

    @Override
    public Identity<AppRecord, Integer> getIdentity() {
        return Keys.IDENTITY_APP;
    }

    @Override
    public UniqueKey<AppRecord> getPrimaryKey() {
        return Keys.CONSTRAINT_F;
    }

    @Override
    public List<UniqueKey<AppRecord>> getKeys() {
        return Arrays.<UniqueKey<AppRecord>>asList(Keys.CONSTRAINT_F);
    }

    @Override
    public List<ForeignKey<AppRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<AppRecord, ?>>asList(Keys.CONSTRAINT_FE);
    }

    public User user() {
        return new User(this, Keys.CONSTRAINT_FE);
    }

    @Override
    public App as(String alias) {
        return new App(DSL.name(alias), this);
    }

    @Override
    public App as(Name alias) {
        return new App(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public App rename(String name) {
        return new App(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public App rename(Name name) {
        return new App(name, null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<Integer, Integer, Boolean, Boolean, Short, Short, Short, Short> fieldsRow() {
        return (Row8) super.fieldsRow();
    }
}
