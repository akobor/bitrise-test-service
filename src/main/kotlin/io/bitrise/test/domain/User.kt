package io.bitrise.test.domain

import com.fasterxml.jackson.annotation.JsonProperty
import io.micronaut.core.annotation.Introspected
import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.NotNull

@Introspected
data class User(
    @field:Schema(name = "userId")
    @JsonProperty("userId")
    @get:NotNull
    val id: Int,
    @get:NotNull
    val planId: Short
)
