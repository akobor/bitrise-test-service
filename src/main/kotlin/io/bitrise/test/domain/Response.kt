package io.bitrise.test.domain

import io.bitrise.test.sql.tables.pojos.PlanPojo
import io.micronaut.core.annotation.Introspected

@Introspected
data class CreateUserResponse(
    val data: User
)

@Introspected
data class AppResponse(
    val data: App
)

@Introspected
data class GetPlansResponse(
    val data: List<PlanPojo>
)
