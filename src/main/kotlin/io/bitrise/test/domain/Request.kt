package io.bitrise.test.domain

import io.micronaut.core.annotation.Introspected
import javax.validation.constraints.NotNull

@Introspected
data class CreateUserRequest(
    @get:NotNull
    val planId: Short? = null
)

@Introspected
data class CreateAppRequest(
    @get:NotNull
    val public: Boolean? = null
)

@Introspected
data class PatchAppLimitRequest(
    override val concurrentBuildLimit: Short?,
    override val buildTimeMinutesLimit: Short?,
    override val monthlyBuildLimit: Short?,
    override val teamMemberLimit: Short?
) : AppLimits

@Introspected
data class PatchAppPlanTypeRequest(
    @get:NotNull
    val usePlanLimits: Boolean? = null
)
