package io.bitrise.test.domain

import io.micronaut.core.annotation.Introspected

@Introspected
data class ServiceError(
    val message: String? = "Something bad happened :("
)

class PlanNotFoundError(
    private val planId: Short,
    override val message: String? = "There is no plan with id: $planId"
) : Throwable()

class UserNotFoundError(
    private val userId: Int,
    override val message: String? = "There is no user with id: $userId"
) : Throwable()

class AppNotFoundError(
    private val appId: Int,
    override val message: String? = "There is no app with id: $appId"
) : Throwable()

class ForbiddenOperationError(
    override val message: String? = "You have no right to do this :("
) : Throwable()
