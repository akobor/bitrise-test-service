package io.bitrise.test.domain

import com.fasterxml.jackson.annotation.JsonProperty
import io.bitrise.test.sql.tables.pojos.AppPojo
import io.bitrise.test.sql.tables.pojos.PlanPojo
import io.micronaut.core.annotation.Introspected
import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.NotNull

@Introspected
data class App(
    @field:Schema(name = "appId")
    @JsonProperty("appId")
    @get:NotNull
    val id: Int,
    @get:NotNull
    val userId: Int,
    @get:NotNull
    val public: Boolean,
    @get:NotNull
    val usePlanLimits: Boolean,
    val concurrentBuildLimit: Short?,
    val buildTimeMinutesLimit: Short?,
    val monthlyBuildLimit: Short?,
    val teamMemberLimit: Short?
) {
    companion object {
        fun fromAppPojoAndLimits(pojo: AppPojo, limits: AppLimits) =
            App(
                id = pojo.id,
                userId = pojo.userId,
                public = pojo.public,
                usePlanLimits = pojo.usePlanLimits,
                concurrentBuildLimit = limits.concurrentBuildLimit,
                monthlyBuildLimit = limits.monthlyBuildLimit,
                buildTimeMinutesLimit = limits.buildTimeMinutesLimit,
                teamMemberLimit = limits.teamMemberLimit
            )

        fun fromAppPojo(pojo: AppPojo) =
            App(
                id = pojo.id,
                userId = pojo.userId,
                public = pojo.public,
                usePlanLimits = pojo.usePlanLimits,
                concurrentBuildLimit = pojo.concurrentBuildLimit,
                monthlyBuildLimit = pojo.monthlyBuildLimit,
                buildTimeMinutesLimit = pojo.buildTimeMinutesLimit,
                teamMemberLimit = pojo.teamMemberLimit
            )

        fun fromAppPojoAndPlanLimits(pojo: AppPojo, plan: PlanPojo) =
            App(
                id = pojo.id,
                userId = pojo.userId,
                public = pojo.public,
                usePlanLimits = pojo.usePlanLimits,
                concurrentBuildLimit = plan.concurrentBuildLimit,
                monthlyBuildLimit = plan.monthlyBuildLimit,
                buildTimeMinutesLimit = plan.buildTimeMinutesLimit,
                teamMemberLimit = plan.teamMemberLimit
            )
    }
}

interface AppLimits {
    val concurrentBuildLimit: Short?
    val buildTimeMinutesLimit: Short?
    val monthlyBuildLimit: Short?
    val teamMemberLimit: Short?
}

object PublicAppDefaultLimits : AppLimits {
    override val concurrentBuildLimit = 2.toShort()
    override val buildTimeMinutesLimit = 45.toShort()
    override val monthlyBuildLimit = null
    override val teamMemberLimit = null
}

object EmptyLimits : AppLimits {
    override val concurrentBuildLimit = null
    override val buildTimeMinutesLimit = null
    override val monthlyBuildLimit = null
    override val teamMemberLimit = null
}
