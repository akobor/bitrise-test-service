package io.bitrise.test.services

import arrow.core.toOption
import io.bitrise.test.domain.PlanNotFoundError
import io.bitrise.test.domain.User
import io.bitrise.test.repositories.PlanRepository
import io.bitrise.test.repositories.UserRepository
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserService @Inject constructor(
    private val userRepository: UserRepository,
    private val planRepository: PlanRepository
) {

    companion object {
        private val logger = LoggerFactory.getLogger(UserService::class.java)
    }

    fun createUser(planId: Short): User =
        planRepository.fetchOneById(planId).toOption().fold(
            {
                val error = PlanNotFoundError(planId)
                logger.debug(error.message)
                throw error
            },
            { plan ->
                val user = userRepository.insertReturning(plan.id)
                logger.debug("User has been successfully created with id: ${user.id}")
                user
            }
        )
}
