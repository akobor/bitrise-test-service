package io.bitrise.test.services

import io.bitrise.test.repositories.PlanRepository
import io.bitrise.test.sql.tables.pojos.PlanPojo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlanService @Inject constructor(
    private val planRepository: PlanRepository
) {

    fun getPlans(): List<PlanPojo> = planRepository.findAll()
}
