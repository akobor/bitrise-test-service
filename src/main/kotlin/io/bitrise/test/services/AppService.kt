package io.bitrise.test.services

import arrow.core.toOption
import io.bitrise.test.domain.App
import io.bitrise.test.domain.AppLimits
import io.bitrise.test.domain.AppNotFoundError
import io.bitrise.test.domain.EmptyLimits
import io.bitrise.test.domain.ForbiddenOperationError
import io.bitrise.test.domain.PublicAppDefaultLimits
import io.bitrise.test.domain.UserNotFoundError
import io.bitrise.test.repositories.AppRepository
import io.bitrise.test.repositories.PlanRepository
import io.bitrise.test.repositories.UserRepository
import io.bitrise.test.sql.tables.pojos.AppPojo
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppService @Inject constructor(
    private val userRepository: UserRepository,
    private val appRepository: AppRepository,
    private val planRepository: PlanRepository
) {

    companion object {
        private val logger = LoggerFactory.getLogger(AppService::class.java)
    }

    fun createAppForUser(userId: Int, public: Boolean): App =
        userRepository.findById(userId).toOption().fold(
            {
                val error = UserNotFoundError(userId)
                logger.debug(error.message)
                throw error
            },
            { user ->
                val limits = if (public) PublicAppDefaultLimits else EmptyLimits
                val appPojo = appRepository.create(user.id, public, !public, limits)
                logger.debug("App has been successfully created with id: ${appPojo.id}")

                appPojo.toAppWithLimits()
            }
        )

    fun getApp(appId: Int): App = findAppPojoById(appId).toAppWithLimits()

    fun updatePublicAppLimits(appId: Int, newLimits: AppLimits): App {
        val appPojo = findAppPojoById(appId).disallowPrivateApps()

        appRepository.updateLimits(appPojo.id, newLimits)
        logger.debug("App's limits has been successfully updated with id: ${appPojo.id}")

        return App.fromAppPojoAndLimits(appPojo, newLimits)
    }

    fun updateAppPlanType(appId: Int, usePlanLimits: Boolean): App {
        val appPojo = findAppPojoById(appId).disallowPrivateApps()
        val newLimits = if (usePlanLimits) EmptyLimits else PublicAppDefaultLimits

        appRepository.updatePlanLimitUsage(appPojo.id, usePlanLimits, newLimits)
        logger.debug("App's plan usage type has been successfully updated with id: ${appPojo.id}")

        return getApp(appId)
    }

    private fun AppPojo.toAppWithLimits(): App =
        if (this.usePlanLimits) {
            val userPlan = planRepository.getPlanByUserId(this.userId).get()
            App.fromAppPojoAndPlanLimits(this, userPlan)
        } else {
            App.fromAppPojo(this)
        }

    private fun AppPojo.disallowPrivateApps(): AppPojo =
        if (!this.public) {
            throw ForbiddenOperationError("App with id: ${this.id} is not a public app")
        } else this

    private fun findAppPojoById(appId: Int): AppPojo =
        appRepository.findById(appId).toOption().fold(
            {
                val error = AppNotFoundError(appId)
                logger.debug(error.message)
                throw error
            },
            { it }
        )
}
