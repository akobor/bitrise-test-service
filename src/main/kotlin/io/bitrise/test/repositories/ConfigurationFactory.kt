package io.bitrise.test.repositories

import io.micronaut.context.annotation.Factory
import org.jooq.Configuration
import org.jooq.SQLDialect
import org.jooq.impl.DefaultConfiguration
import javax.inject.Inject
import javax.inject.Singleton
import javax.sql.DataSource

@Factory
class ConfigurationFactory @Inject constructor(private val dataSource: DataSource) {

    @Singleton
    fun configuration(): Configuration =
        DefaultConfiguration().apply {
            setSQLDialect(SQLDialect.H2)
            set(dataSource)
        }
}
