package io.bitrise.test.repositories

import io.bitrise.test.domain.User
import io.bitrise.test.sql.tables.User.USER
import io.bitrise.test.sql.tables.daos.UserDao
import io.bitrise.test.sql.tables.records.UserRecord
import org.jooq.Configuration
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(config: Configuration) : UserDao(config) {

    private val ctx = config.dsl()

    fun insertReturning(planId: Short): User {
        val record = UserRecord().setPlanId(planId)
        return ctx.insertInto(USER)
            .set(record)
            .returning(USER.asterisk())
            .fetchOne()
            .into(User::class.java)
    }
}
