package io.bitrise.test.repositories

import io.bitrise.test.sql.tables.Plan.PLAN
import io.bitrise.test.sql.tables.User.USER
import io.bitrise.test.sql.tables.daos.PlanDao
import io.bitrise.test.sql.tables.pojos.PlanPojo
import org.jooq.Configuration
import java.util.Optional
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlanRepository @Inject constructor(config: Configuration) : PlanDao(config) {

    private val ctx = config.dsl()

    fun getPlanByUserId(userId: Int): Optional<PlanPojo> =
        ctx.select(PLAN.asterisk())
            .from(PLAN)
            .join(USER).on(USER.PLAN_ID.eq(PLAN.ID))
            .where(USER.ID.eq(userId))
            .fetchOptionalInto(PlanPojo::class.java)
}
