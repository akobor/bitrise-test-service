package io.bitrise.test.repositories

import io.bitrise.test.domain.AppLimits
import io.bitrise.test.sql.tables.App.APP
import io.bitrise.test.sql.tables.daos.AppDao
import io.bitrise.test.sql.tables.pojos.AppPojo
import io.bitrise.test.sql.tables.records.AppRecord
import org.jooq.Configuration
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepository @Inject constructor(config: Configuration) : AppDao(config) {

    private val ctx = config.dsl()

    fun create(userId: Int, public: Boolean, usePlanLimits: Boolean, limits: AppLimits): AppPojo {
        val record = AppRecord()
            .setUserId(userId)
            .setPublic(public)
            .setUsePlanLimits(usePlanLimits)
            .setConcurrentBuildLimit(limits.concurrentBuildLimit)
            .setBuildTimeMinutesLimit(limits.buildTimeMinutesLimit)
            .setMonthlyBuildLimit(limits.monthlyBuildLimit)
            .setTeamMemberLimit(limits.teamMemberLimit)
        return ctx.insertInto(APP)
            .set(record)
            .returning(APP.asterisk())
            .fetchOne()
            .into(AppPojo::class.java)
    }

    fun updateLimits(appId: Int, newLimits: AppLimits) {
        val record = AppRecord()
            .setConcurrentBuildLimit(newLimits.concurrentBuildLimit)
            .setBuildTimeMinutesLimit(newLimits.buildTimeMinutesLimit)
            .setMonthlyBuildLimit(newLimits.monthlyBuildLimit)
            .setTeamMemberLimit(newLimits.teamMemberLimit)

        ctx.update(APP)
            .set(record)
            .where(APP.ID.eq(appId))
            .execute()
    }

    fun updatePlanLimitUsage(appId: Int, usePlanLimits: Boolean, newLimits: AppLimits) {
        val record = AppRecord()
            .setUsePlanLimits(usePlanLimits)
            .setConcurrentBuildLimit(newLimits.concurrentBuildLimit)
            .setBuildTimeMinutesLimit(newLimits.buildTimeMinutesLimit)
            .setMonthlyBuildLimit(newLimits.monthlyBuildLimit)
            .setTeamMemberLimit(newLimits.teamMemberLimit)

        ctx.update(APP)
            .set(record)
            .where(APP.ID.eq(appId))
            .execute()
    }
}
