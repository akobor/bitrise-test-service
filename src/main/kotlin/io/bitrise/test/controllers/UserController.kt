package io.bitrise.test.controllers

import io.bitrise.test.domain.CreateAppRequest
import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.CreateUserRequest
import io.bitrise.test.domain.CreateUserResponse
import io.bitrise.test.domain.ServiceError
import io.bitrise.test.services.AppService
import io.bitrise.test.services.UserService
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Status
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import javax.inject.Inject

@Controller("/user", produces = [MediaType.APPLICATION_JSON])
@Tag(name = "User Operations")
class UserController @Inject constructor(
    private val userService: UserService,
    private val appService: AppService
) : UserOperations {

    @Status(HttpStatus.CREATED)
    @ApiResponses(
        ApiResponse(
            responseCode = "201",
            description = "Successful creation",
            content = [Content(schema = Schema(implementation = CreateUserResponse::class))]
        ),
        ApiResponse(
            responseCode = "400",
            description = "Bad request",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        )
    )
    override fun createUser(createUserRequest: CreateUserRequest): CreateUserResponse {
        val user = userService.createUser(createUserRequest.planId!!)
        return CreateUserResponse(user)
    }

    @Status(HttpStatus.CREATED)
    @ApiResponses(
        ApiResponse(
            responseCode = "201",
            description = "Successful creation",
            content = [Content(schema = Schema(implementation = AppResponse::class))]
        ),
        ApiResponse(
            responseCode = "400",
            description = "Bad request",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        )
    )
    override fun createAppForUser(userId: Int, createAppRequest: CreateAppRequest): AppResponse {
        val app = appService.createAppForUser(userId, createAppRequest.public!!)
        return AppResponse(app)
    }
}
