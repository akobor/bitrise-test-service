package io.bitrise.test.controllers

import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.PatchAppLimitRequest
import io.bitrise.test.domain.PatchAppPlanTypeRequest
import io.bitrise.test.domain.ServiceError
import io.bitrise.test.services.AppService
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import javax.inject.Inject

@Controller("/app", produces = [MediaType.APPLICATION_JSON])
@Tag(name = "App Operations")
class AppController @Inject constructor(
    private val appService: AppService
) : AppOperations {

    @ApiResponses(
        ApiResponse(
            responseCode = "200",
            description = "Successful update",
            content = [Content(schema = Schema(implementation = AppResponse::class))]
        ),
        ApiResponse(
            responseCode = "400",
            description = "Bad request",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        ),
        ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        ),
        ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        )
    )
    override fun patchPublicAppLimit(appId: Int, patchAppLimitRequest: PatchAppLimitRequest): AppResponse {
        val app = appService.updatePublicAppLimits(appId, patchAppLimitRequest)
        return AppResponse(app)
    }

    @ApiResponses(
        ApiResponse(
            responseCode = "200",
            description = "Successful query",
            content = [Content(schema = Schema(implementation = AppResponse::class))]
        ),
        ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        )
    )
    override fun getApp(appId: Int): AppResponse {
        val app = appService.getApp(appId)
        return AppResponse(app)
    }

    @ApiResponses(
        ApiResponse(
            responseCode = "200",
            description = "Successful update",
            content = [Content(schema = Schema(implementation = AppResponse::class))]
        ),
        ApiResponse(
            responseCode = "400",
            description = "Bad request",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        ),
        ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        ),
        ApiResponse(
            responseCode = "404",
            description = "Not found",
            content = [Content(schema = Schema(implementation = ServiceError::class))]
        )
    )
    override fun patchPublicAppPlanType(appId: Int, patchAppPlanTypeRequest: PatchAppPlanTypeRequest): AppResponse {
        val app = appService.updateAppPlanType(appId, patchAppPlanTypeRequest.usePlanLimits!!)
        return AppResponse(app)
    }
}
