package io.bitrise.test.controllers

import io.bitrise.test.domain.CreateAppRequest
import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.CreateUserRequest
import io.bitrise.test.domain.CreateUserResponse
import io.micronaut.core.version.annotation.Version
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Post
import io.micronaut.validation.Validated
import io.swagger.v3.oas.annotations.Operation
import javax.validation.Valid

@Validated
interface UserOperations {

    @Version("1")
    @Operation(summary = "Creates a user on the given plan")
    @Post("/")
    fun createUser(@Valid @Body createUserRequest: CreateUserRequest): CreateUserResponse

    @Version("1")
    @Operation(summary = "Creates an app for the given user")
    @Post("/{userId}/app")
    fun createAppForUser(userId: Int, @Valid @Body createAppRequest: CreateAppRequest): AppResponse
}
