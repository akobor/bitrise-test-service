package io.bitrise.test.controllers

import io.bitrise.test.domain.GetPlansResponse
import io.bitrise.test.services.PlanService
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import javax.inject.Inject

@Controller("/plan", produces = [MediaType.APPLICATION_JSON])
@Tag(name = "Plan Operations")
class PlanController @Inject constructor(
    private val planService: PlanService
) : PlanOperations {

    @ApiResponses(
        ApiResponse(
            responseCode = "200",
            description = "Successful query",
            content = [Content(schema = Schema(implementation = GetPlansResponse::class))]
        )
    )
    override fun getPlans(): GetPlansResponse {
        val plans = planService.getPlans()
        return GetPlansResponse(plans)
    }
}
