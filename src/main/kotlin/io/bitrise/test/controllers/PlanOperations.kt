package io.bitrise.test.controllers

import io.bitrise.test.domain.GetPlansResponse
import io.micronaut.core.version.annotation.Version
import io.micronaut.http.annotation.Get
import io.micronaut.validation.Validated
import io.swagger.v3.oas.annotations.Operation

@Validated
interface PlanOperations {

    @Version("1")
    @Operation(summary = "Returns all the available plans")
    @Get("/")
    fun getPlans(): GetPlansResponse
}
