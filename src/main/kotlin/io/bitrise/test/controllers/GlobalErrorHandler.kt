package io.bitrise.test.controllers

import com.fasterxml.jackson.core.JsonParseException
import io.bitrise.test.domain.AppNotFoundError
import io.bitrise.test.domain.ForbiddenOperationError
import io.bitrise.test.domain.PlanNotFoundError
import io.bitrise.test.domain.ServiceError
import io.bitrise.test.domain.UserNotFoundError
import io.micronaut.core.convert.exceptions.ConversionErrorException
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Error
import org.slf4j.LoggerFactory
import javax.validation.ValidationException

@Controller
class GlobalErrorHandler {

    companion object {
        private val logger = LoggerFactory.getLogger(GlobalErrorHandler::class.java)
    }

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun jsonError(request: HttpRequest<*>, planNotFoundError: PlanNotFoundError): HttpResponse<ServiceError> {
        val error = ServiceError(planNotFoundError.message)
        return HttpResponse.badRequest(error)
    }

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun jsonError(request: HttpRequest<*>, userNotFoundError: UserNotFoundError): HttpResponse<ServiceError> {
        val error = ServiceError(userNotFoundError.message)
        return HttpResponse.notFound(error)
    }

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun jsonError(request: HttpRequest<*>, appNotFoundError: AppNotFoundError): HttpResponse<ServiceError> {
        val error = ServiceError(appNotFoundError.message)
        return HttpResponse.notFound(error)
    }

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun error(request: HttpRequest<*>, throwable: ValidationException): HttpResponse<ServiceError> =
        HttpResponse.badRequest(ServiceError(throwable.message))

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun error(request: HttpRequest<*>, throwable: ConversionErrorException): HttpResponse<ServiceError> {
        val message = "Failed to convert argument: ${throwable.argument}"
        return HttpResponse.badRequest(ServiceError(message))
    }

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun error(request: HttpRequest<*>, throwable: JsonParseException): HttpResponse<ServiceError> {
        val message = "Can't parse the JSON in the payload"
        return HttpResponse.badRequest(ServiceError(message))
    }

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun error(request: HttpRequest<*>, throwable: ForbiddenOperationError): HttpResponse<ServiceError> =
        HttpResponse
            .status<ServiceError>(HttpStatus.FORBIDDEN)
            .body(ServiceError(throwable.message))

    @Suppress("UNUSED_PARAMETER")
    @Error(global = true)
    fun error(request: HttpRequest<*>, throwable: Throwable): HttpResponse<ServiceError> {
        logger.error("Global error handler", throwable)
        return HttpResponse.serverError(ServiceError())
    }
}
