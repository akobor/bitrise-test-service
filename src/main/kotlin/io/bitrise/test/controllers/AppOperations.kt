package io.bitrise.test.controllers

import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.PatchAppLimitRequest
import io.bitrise.test.domain.PatchAppPlanTypeRequest
import io.micronaut.core.version.annotation.Version
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Patch
import io.micronaut.validation.Validated
import io.swagger.v3.oas.annotations.Operation
import javax.validation.Valid

@Validated
interface AppOperations {

    @Version("1")
    @Operation(summary = "Updates a public app's limits")
    @Patch("/public/{appId}/limit")
    fun patchPublicAppLimit(appId: Int, @Valid @Body patchAppLimitRequest: PatchAppLimitRequest): AppResponse

    @Version("1")
    @Operation(summary = "Returns an app with the given ID")
    @Get("/{appId}")
    fun getApp(appId: Int): AppResponse

    @Version("1")
    @Operation(summary = "Updates a public app's plan usage type")
    @Patch("/public/{appId}/plan-type")
    fun patchPublicAppPlanType(appId: Int, @Valid @Body patchAppPlanTypeRequest: PatchAppPlanTypeRequest): AppResponse
}
