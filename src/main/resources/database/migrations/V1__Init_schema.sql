CREATE TABLE plan
(
    id                       smallint    not null auto_increment,
    name                     varchar(50) not null,
    concurrent_build_limit   smallint    not null,
    build_time_minutes_limit smallint    not null,
    monthly_build_limit      smallint,
    team_member_limit        smallint,
    primary key (id)
);

CREATE TABLE user
(
    id      int      not null auto_increment,
    plan_id smallint not null,
    primary key (id),
    foreign key (plan_id) references plan (id)
);

CREATE INDEX user_plan_id ON user (plan_id);

CREATE TABLE app
(
    id                       int     not null auto_increment,
    user_id                  int     not null,
    public                   boolean not null default false,
    use_plan_limits          boolean not null default true,
    concurrent_build_limit   smallint,
    build_time_minutes_limit smallint,
    monthly_build_limit      smallint,
    team_member_limit        smallint,
    primary key (id),
    foreign key (user_id) references user (id)
);

CREATE INDEX app_user_id ON app (user_id);
