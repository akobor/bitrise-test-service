INSERT INTO plan (name, concurrent_build_limit, build_time_minutes_limit, monthly_build_limit, team_member_limit)
VALUES ('Free', 1, 10, 200, 2);
INSERT INTO plan (name, concurrent_build_limit, build_time_minutes_limit, monthly_build_limit, team_member_limit)
VALUES ('Developer', 2, 45, null, null);
INSERT INTO plan (name, concurrent_build_limit, build_time_minutes_limit, monthly_build_limit, team_member_limit)
VALUES ('Organization', 4, 90, null, null);
