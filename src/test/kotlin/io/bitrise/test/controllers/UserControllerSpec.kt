package io.bitrise.test.controllers

import io.bitrise.test.domain.CreateAppRequest
import io.bitrise.test.domain.CreateUserRequest
import io.bitrise.test.domain.PublicAppDefaultLimits
import io.bitrise.test.mocks.JsonMocks
import io.bitrise.test.repositories.PlanRepository
import io.bitrise.test.utils.assertLimits
import io.bitrise.test.utils.assertThrownException
import io.bitrise.test.utils.getBean
import io.bitrise.test.utils.getLowLevelClient
import io.bitrise.test.utils.resetDatabase
import io.bitrise.test.utils.startTestApplication
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus

class UserControllerSpec : BehaviorSpec() {
    private val application = autoClose(startTestApplication())
    private val userClient = application.getBean<UserClient>()
    private val httpClient = application.getLowLevelClient()
    private val planRepository = application.getBean<PlanRepository>()

    override fun afterTest(testCase: TestCase, result: TestResult) {
        application.resetDatabase()
    }

    init {
        given("a createUser endpoint") {
            `when`("a valid request comes in") {
                val planId = 2.toShort()
                val request = CreateUserRequest(planId)
                val response = userClient.createUser(request)
                then("it should return the created User") {
                    response.data.id.shouldNotBeNull()
                    response.data.planId.shouldBe(planId)
                }
            }
            `when`("a non-existent planId comes in") {
                val request = HttpRequest.POST("/user", JsonMocks.createUserRequestWithNonExistentPlan)
                then("it should return 400 because of a PlanNotFoundError") {
                    httpClient.assertThrownException(request, HttpStatus.BAD_REQUEST, "There is no plan with id:")
                }
            }
            `when`("a malformatted planId comes in") {
                val request = HttpRequest.POST("/user", JsonMocks.createUserRequestWithBadPlanType)
                then("it should return 400 because of a ConversionErrorException") {
                    httpClient.assertThrownException(
                        request,
                        HttpStatus.BAD_REQUEST,
                        "Failed to convert argument:"
                    )
                }
            }
            `when`("a planId with null value comes in") {
                val request = HttpRequest.POST("/user", JsonMocks.createUserRequestWithNullPlanId)
                then("it should return 400 because of a ValidationException") {
                    httpClient.assertThrownException(request, HttpStatus.BAD_REQUEST, "planId: must not be null")
                }
            }
        }

        given("a createAppForUser endpoint") {
            `when`("a valid request comes in for a public app") {
                val userId = createUser()
                val public = true
                val request = CreateAppRequest(public)
                val response = userClient.createAppForUser(userId, request)
                then("it should return the created app with correct limits") {
                    response.data.id.shouldNotBeNull()
                    response.data.public.shouldBe(public)
                    response.data.usePlanLimits.shouldBe(false)
                    response.data.assertLimits(PublicAppDefaultLimits)
                }
            }
            `when`("a valid request comes in for a private app") {
                val userId = createUser()
                val public = false
                val request = CreateAppRequest(public)
                val response = userClient.createAppForUser(userId, request)
                then("it should return the created app with correct limits") {
                    val appId = response.data.id
                    appId.shouldNotBeNull()
                    response.data.public.shouldBe(public)
                    response.data.usePlanLimits.shouldBe(true)

                    val userPlan = planRepository.getPlanByUserId(userId).get()
                    response.data.assertLimits(userPlan)
                }
            }
            `when`("a non-existent userId comes in") {
                val request = HttpRequest.POST("/user/9999999/app", JsonMocks.createAppRequestWithValidBody)
                then("it should return 400 because of a UserNotFoundError") {
                    httpClient.assertThrownException(request, HttpStatus.NOT_FOUND, "There is no user with id:")
                }
            }
            `when`("`public` equals null in the request body") {
                val userId = createUser()
                val request = HttpRequest.POST("/user/$userId/app", JsonMocks.createAppRequestWithNullPublicProperty)
                then("it should return 400 because of a ValidationException") {
                    httpClient.assertThrownException(request, HttpStatus.BAD_REQUEST, "public: must not be null")
                }
            }
            `when`("`public` is not a boolean in the request body") {
                val userId = createUser()
                val request = HttpRequest.POST("/user/$userId/app", JsonMocks.createAppRequestWithBadPublicType)
                then("it should return 400 because of a ConversionErrorException") {
                    httpClient.assertThrownException(
                        request,
                        HttpStatus.BAD_REQUEST,
                        "Failed to convert argument:"
                    )
                }
            }
        }
    }

    private fun createUser(): Int = userClient.createUser(CreateUserRequest(1)).data.id
}
