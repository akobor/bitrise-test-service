package io.bitrise.test.controllers

import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.PatchAppLimitRequest
import io.bitrise.test.domain.PatchAppPlanTypeRequest
import io.micronaut.http.client.annotation.Client

@Client("/app")
interface AppClient : AppOperations {
    override fun patchPublicAppLimit(appId: Int, patchAppLimitRequest: PatchAppLimitRequest): AppResponse

    override fun getApp(appId: Int): AppResponse

    override fun patchPublicAppPlanType(appId: Int, patchAppPlanTypeRequest: PatchAppPlanTypeRequest): AppResponse
}
