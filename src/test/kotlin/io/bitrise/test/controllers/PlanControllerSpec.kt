package io.bitrise.test.controllers

import io.bitrise.test.utils.getBean
import io.bitrise.test.utils.startTestApplication
import io.kotlintest.matchers.numerics.shouldBeGreaterThan
import io.kotlintest.specs.BehaviorSpec

class PlanControllerSpec : BehaviorSpec() {
    private val application = autoClose(startTestApplication())
    private val planClient = application.getBean<PlanClient>()

    init {
        given("a getPlans endpoint") {
            `when`("a valid request comes in") {
                val response = planClient.getPlans()
                then("it should return the available plans") {
                    response.data.size.shouldBeGreaterThan(0)
                }
            }
        }
    }
}
