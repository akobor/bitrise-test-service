package io.bitrise.test.controllers

import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.CreateAppRequest
import io.bitrise.test.domain.CreateUserRequest
import io.bitrise.test.domain.PatchAppLimitRequest
import io.bitrise.test.domain.PatchAppPlanTypeRequest
import io.bitrise.test.domain.PublicAppDefaultLimits
import io.bitrise.test.repositories.PlanRepository
import io.bitrise.test.utils.assertLimits
import io.bitrise.test.utils.assertThrownException
import io.bitrise.test.utils.getBean
import io.bitrise.test.utils.getLowLevelClient
import io.bitrise.test.utils.resetDatabase
import io.bitrise.test.utils.startTestApplication
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.exceptions.HttpClientResponseException

class AppControllerSpec : BehaviorSpec() {
    private val application = autoClose(startTestApplication())
    private val userClient = application.getBean<UserClient>()
    private val appClient = application.getBean<AppClient>()
    private val planRepository = application.getBean<PlanRepository>()
    private val httpClient = application.getLowLevelClient()

    override fun afterTest(testCase: TestCase, result: TestResult) {
        application.resetDatabase()
    }

    init {
        given("a patchAppLimit endpoint") {

            `when`("a valid request comes in for a public app") {
                val request = getValidPatchLimitRequest()
                val appId = createPublicApp(createUser())
                val response = appClient.patchPublicAppLimit(appId, request)
                then("it should return the updated App") {
                    response.data.id.shouldBe(appId)
                    response.data.public.shouldBe(true)
                    response.data.usePlanLimits.shouldBe(false)
                    response.data.assertLimits(request)
                }
            }

            `when`("a valid request comes in for a private app") {
                val request = getValidPatchLimitRequest()
                val appId = createPrivateApp(createUser())
                then("it should return 403 - Forbidden") {
                    val exception = shouldThrow<HttpClientResponseException> {
                        appClient.patchPublicAppLimit(appId, request)
                    }
                    exception.status.shouldBe(HttpStatus.FORBIDDEN)
                }
            }

            `when`("a valid request comes in for a non-existent app") {
                val patchRequest = getValidPatchLimitRequest()
                val appId = -1
                val request = HttpRequest.PATCH("/app/public/$appId/limit", patchRequest)
                then("it should return 404 because of an AppNotFoundError") {
                    httpClient.assertThrownException(request, HttpStatus.NOT_FOUND, "There is no app with id:")
                }
            }
        }

        given("a getApp endpoint") {

            `when`("a valid request comes in for a public app") {
                val appId = createPublicApp(createUser())
                val response = appClient.getApp(appId)
                then("it should return the app with default limits") {
                    response.data.id.shouldBe(appId)
                    response.data.public.shouldBe(true)
                    response.data.usePlanLimits.shouldBe(false)
                    response.data.assertLimits(PublicAppDefaultLimits)
                }
            }

            `when`("a valid request comes in for a private app") {
                val userId = createUser()
                val appId = createPrivateApp(userId)
                val response = appClient.getApp(appId)
                then("it should return the app with correct limits") {
                    response.data.id.shouldBe(appId)
                    response.data.public.shouldBe(false)
                    response.data.usePlanLimits.shouldBe(true)

                    val userPlan = planRepository.getPlanByUserId(userId).get()
                    response.data.assertLimits(userPlan)
                }
            }

            `when`("a valid request comes in for a non-existent app") {
                val appId = -1
                val request = HttpRequest.GET<AppResponse>("/app/$appId")
                then("it should return 404 because of an AppNotFoundError") {
                    httpClient.assertThrownException(request, HttpStatus.NOT_FOUND, "There is no app with id:")
                }
            }
        }

        given("a patchAppType endpoint") {

            `when`("a valid request comes in for a public app with default settings") {
                val request = getValidPatchPlanTypeRequest()
                val userId = createUser()
                val appId = createPublicApp(userId)
                val response = appClient.patchPublicAppPlanType(appId, request)
                then("it should return the limits from the user's plan") {
                    response.data.id.shouldBe(appId)
                    response.data.public.shouldBe(true)
                    response.data.usePlanLimits.shouldBe(request.usePlanLimits)

                    val userPlan = planRepository.getPlanByUserId(userId).get()
                    response.data.assertLimits(userPlan)
                }
            }

            `when`("a valid request comes in for a public app with updated plan usage type") {
                val appId = createPublicApp(createUser())
                // Set "usePlanLimit" to true
                appClient.patchPublicAppPlanType(appId, getValidPatchPlanTypeRequest())

                val planTypeRequest = PatchAppPlanTypeRequest(false)
                // Set "usePlanLimit" back to false
                val response = appClient.patchPublicAppPlanType(appId, planTypeRequest)
                then("it should return the default public app limits") {
                    response.data.id.shouldBe(appId)
                    response.data.public.shouldBe(true)
                    response.data.usePlanLimits.shouldBe(planTypeRequest.usePlanLimits)
                    response.data.assertLimits(PublicAppDefaultLimits)
                }
            }

            `when`("a valid request comes in for a private app") {
                val request = getValidPatchPlanTypeRequest()
                val appId = createPrivateApp(createUser())
                then("it should return 403 - Forbidden") {
                    val exception = shouldThrow<HttpClientResponseException> {
                        appClient.patchPublicAppPlanType(appId, request)
                    }
                    exception.status.shouldBe(HttpStatus.FORBIDDEN)
                }
            }

            `when`("a valid request comes in for a non-existent app") {
                val patchRequest = getValidPatchPlanTypeRequest()
                val appId = -1
                val request = HttpRequest.PATCH("/app/public/$appId/plan-type", patchRequest)
                then("it should return 404 because of an AppNotFoundError") {
                    httpClient.assertThrownException(request, HttpStatus.NOT_FOUND, "There is no app with id:")
                }
            }
        }
    }

    private fun createUser(): Int = userClient.createUser(CreateUserRequest(1)).data.id
    private fun createPublicApp(userId: Int): Int = userClient.createAppForUser(userId, CreateAppRequest(true)).data.id
    private fun createPrivateApp(userId: Int): Int =
        userClient.createAppForUser(userId, CreateAppRequest(false)).data.id

    private fun getValidPatchLimitRequest() =
        PatchAppLimitRequest(
            concurrentBuildLimit = 1.toShort(),
            monthlyBuildLimit = 2.toShort(),
            teamMemberLimit = 3.toShort(),
            buildTimeMinutesLimit = 4.toShort()
        )

    private fun getValidPatchPlanTypeRequest() = PatchAppPlanTypeRequest(usePlanLimits = true)
}
