package io.bitrise.test.controllers

import io.bitrise.test.domain.GetPlansResponse
import io.micronaut.http.client.annotation.Client

@Client("/plan")
interface PlanClient : PlanOperations {
    override fun getPlans(): GetPlansResponse
}
