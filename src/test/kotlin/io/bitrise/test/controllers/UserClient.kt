package io.bitrise.test.controllers

import io.bitrise.test.domain.CreateAppRequest
import io.bitrise.test.domain.AppResponse
import io.bitrise.test.domain.CreateUserRequest
import io.bitrise.test.domain.CreateUserResponse
import io.micronaut.http.client.annotation.Client

@Client("/user")
interface UserClient : UserOperations {
    override fun createAppForUser(userId: Int, createAppRequest: CreateAppRequest): AppResponse

    override fun createUser(createUserRequest: CreateUserRequest): CreateUserResponse
}
