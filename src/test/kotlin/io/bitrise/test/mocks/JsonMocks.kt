package io.bitrise.test.mocks

object JsonMocks {
    const val createUserRequestWithBadPlanType = """{"planId": "irrelevant"}"""
    const val createUserRequestWithNonExistentPlan = """{"planId": "-1"}"""
    const val createUserRequestWithNullPlanId = """{"planId": null}"""
    const val createAppRequestWithValidBody = """{"public": true}"""
    const val createAppRequestWithNullPublicProperty = """{"public": null}"""
    const val createAppRequestWithBadPublicType = """{"public": "irrelevant"}"""
}
