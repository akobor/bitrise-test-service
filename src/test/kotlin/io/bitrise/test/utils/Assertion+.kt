package io.bitrise.test.utils

import io.bitrise.test.domain.App
import io.bitrise.test.domain.AppLimits
import io.bitrise.test.domain.ServiceError
import io.bitrise.test.sql.tables.pojos.PlanPojo
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.micronaut.http.HttpStatus
import io.micronaut.http.MutableHttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException

fun RxHttpClient.assertThrownException(
    request: MutableHttpRequest<*>,
    expectedStatus: HttpStatus,
    expectedMessage: String
) {
    val exception = shouldThrow<HttpClientResponseException> {
        this.exchange(
            request,
            ServiceError::class.java
        ).blockingFirst()
    }
    exception.status.shouldBe(expectedStatus)
    exception.message.shouldContain(expectedMessage)
}

fun App.assertLimits(expected: AppLimits) {
    this.concurrentBuildLimit.shouldBe(expected.concurrentBuildLimit)
    this.monthlyBuildLimit.shouldBe(expected.monthlyBuildLimit)
    this.teamMemberLimit.shouldBe(expected.teamMemberLimit)
    this.buildTimeMinutesLimit.shouldBe(expected.buildTimeMinutesLimit)
}

fun App.assertLimits(expected: PlanPojo) {
    this.concurrentBuildLimit.shouldBe(expected.concurrentBuildLimit)
    this.monthlyBuildLimit.shouldBe(expected.monthlyBuildLimit)
    this.teamMemberLimit.shouldBe(expected.teamMemberLimit)
    this.buildTimeMinutesLimit.shouldBe(expected.buildTimeMinutesLimit)
}
