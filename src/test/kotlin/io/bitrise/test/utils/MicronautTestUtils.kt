package io.bitrise.test.utils

import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import org.flywaydb.core.Flyway

fun startTestApplication(): EmbeddedServer =
    ApplicationContext
        .build()
        .build()
        .start()
        .getBean(EmbeddedServer::class.java)
        .start()

inline fun <reified T : Any> EmbeddedServer.getBean(): T = this.applicationContext.getBean(T::class.java)

fun EmbeddedServer.getLowLevelClient(): RxHttpClient =
    this.applicationContext.createBean(RxHttpClient::class.java, this.url)

fun EmbeddedServer.resetDatabase() =
    this.getBean<Flyway>().let { flyway ->
        flyway.clean()
        flyway.migrate()
    }
