[![pipeline status](https://gitlab.com/akobor/bitrise-test-service/badges/master/pipeline.svg)](https://gitlab.com/akobor/bitrise-test-service/-/commits/master)
[![coverage report](https://gitlab.com/akobor/bitrise-test-service/badges/master/coverage.svg)](https://gitlab.com/akobor/bitrise-test-service/-/commits/master)

# bitrise-test-service

This is my solution for Bitrise's backend engineer hiring task.

[Demo / OpenAPI 3.0 API doc](http://35.195.226.15:8080/swagger-ui)

## Tech stack

- Language: Kotlin
- Framework: Micronaut (with a built-in Netty server/client)
- Build tool: Gradle
- Test lib: KotlinTest with JUnit5 runner
- Data Access, Persistency: H2 in-memory database + jOOQ
- API docs: OpenAPI + SwaggerUI (generated build time from source)
- Schema migration: Flyway

## Prerequisites for local running/development

- JDK 8 

You can find a few helpful command in a Makefile, so if you run `make help` in the root of the project, you'll get some information about them.

## CI/CD

The application is deployed to GKE through GitLab CI (see `.gitlab-ci.yml`), which is also taking care of the static code analysis (Detekt) and testing, whenever somebody pushes something to the `master` branch (test step will be initiated for every branch). 

## Short functional summary

You can find the core business logic in `src/main/kotlin/io/bitrise/test/services`. "Above" that you'll find very thin HTTP controllers, "below" that a simple DAL.

If you want to test for the requirements of the task, you can do it over HTTP:

- "create a user account on one of the plans" -> `POST /user`
- "create a public or private app for a user" -> `POST /user/{userId}/app`
- "set up custom limits for a public app" -> `PATCH /app/public/{appId}/limit`
- "opt out from the default public app limits" -> `PATCH /app/public/{appId}/plan-type`
- "get the limits of an app" -> not just the limits, but the whole app, because I think it's more REST-like and conventional -> `GET /app/{appId}`

It was out of the scope, but could be helpful when testing: you can also get the available plans through `GET /plan`.
 
## Room for improvement

**Use Jib for Docker image building**

Faster, reproducible builds with efficient layer caching, without depending on Docker daemon.

**Migrate build.gradle to build.gradle.kts to use Kotlin DSL instead of Groovy**

Needs some ugly boilerplate to make it work with the current stack.

**Prefer unit tests over E2E tests (according to a proper test pyramid)**

It's a fortunate scenario, when we have a simple microservice like this, and we get almost full coverage writing relatively simple E2E tests. However in a more complicated service it would be more convenient and maintainable to prefer "cheap" unit tests, where it's applicable.
